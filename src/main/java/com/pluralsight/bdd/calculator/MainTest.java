package com.pluralsight.bdd.calculator;

import java.util.Locale;
import java.util.function.Function;

public class MainTest {

    public static void main(String[] args) {
        Function<String, String> f1 = str -> {
            return str.toUpperCase(Locale.ROOT);
        };

        System.out.println(f1.apply("test"));
    }
}
