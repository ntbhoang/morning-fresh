package com.pluralsight.bdd.loyaty_card;

import com.pluralsight.bdd.loyalty_card.DrinkCatalog;
import com.pluralsight.bdd.loyalty_card.MorningFreshnessMember;
import com.pluralsight.bdd.loyalty_card.SuperSmoothieSchema;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


public class SuperSmoothieProgramStepDefinitions {

    private DrinkCatalog drinkCatalog = new DrinkCatalog();
    private SuperSmoothieSchema superSmoothieSchema = new SuperSmoothieSchema(drinkCatalog);
    private MorningFreshnessMember james;

    @Given("the following drink categories:")
    public void the_following_drink_categories(List<Map<String, String>> drinkCategories) {
        drinkCategories.stream()
                       .forEach(
                               drinkCategory -> {
                                   String drink = drinkCategory.get("Drink");
                                   String category = drinkCategory.get("Category");
                                   Integer points = Integer.parseInt(drinkCategory.get("Points"));
                                   drinkCatalog.addDrink(drink, category);
                                   superSmoothieSchema.setPointsPerCategory(category, points);
                });

    }
    @Given("^(.*) is a Morning Freshness Member$")
    public void james_is_a_morning_fresher_member(String name) {
        james = new MorningFreshnessMember(name, superSmoothieSchema);
    }
    @When("^(.*) purchases (\\d+) (.*) drinks?")
    public void james_purchases_drinks(String name, Integer quantity, String drink) {
        james.orders(quantity, drink);
    }
    @Then("he should earn {int} points")
    public void he_should_earn_points(Integer expectedPoints) {
        assertThat(james.getPoints())
                .isEqualTo(expectedPoints);
    }

}
