package com.pluralsight.bdd.loyaty_card;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;



@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:com.pluralsight.bdd/features/loyalty_cards",
        glue = "com.pluralsight.bdd"
)
public class SuperSmoothieProgramTestSuite {
}
