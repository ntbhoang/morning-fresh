package com.pluralsight.bdd;

import com.pluralsight.bdd.calculator.Calculator;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class CalculatorStepDefinitions {
    Calculator calculator;
    int result;

    @Given("I have a calculator")
    public void i_have_a_calculator() {
       calculator = new Calculator();
    }
    @When("I add {int} and {int}")
    public void i_add_and(Integer int1, Integer int2) {
        result = calculator.add(1, 2);
    }
    @Then("I should get {int}")
    public void i_should_get(Integer int1) {
        assertThat(result).isEqualTo(3);
    }
}
